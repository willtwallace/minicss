import re
import sys



gfn = raw_input("Enter original file (without extension): ")

if str(gfn) == "" :
	sys.exit("\n\n\n!!!Original filename cannot be blank! Program Terminated (0)\n\n\n")



fn = raw_input("Enter new filename (leave blank for default): ")

if str(fn) == "" :
	fn = gfn

	

with open(gfn + ".css") as f:
	data = f.read()
	data = re.sub(r"\s+", "", data)

	m = open(fn + ".min.css", "w")
	m.write(data)
	m.close

	print("\nOK... Done, Goodbye!\n")
